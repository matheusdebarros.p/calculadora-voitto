import React from 'react';
import './assets/styles/index.css';
import './assets/styles/calc.css';

export default class App extends React.Component {
    state = {
        visor: 0,
        primeiroValor: null,
        segundoValor: null,
        operacao: null,
        resultado: 0
    }

    jogarNoDisplay = (ev) => {
        console.log('Jogando no display!', ev.target.value)
        let visor = this.state.visor
        if (visor == 0) {
            visor = ev.target.value.toString()
        } else {
            visor = this.state.visor + ev.target.value.toString();
        }

        this.setState({ visor: visor })
    }

    operacao = (ev) => {
        console.log('Operação!', ev.target.value)
        let operacao = ev.target.value
        this.setState({ operacao: operacao })

        this.setState({ primeiroValor: this.state.visor })
        this.limparVisor()

        // Bloquear outras operações
    }

    finalizarCalculo = () => {
        this.setState({ segundoValor: this.state.visor }, () => {
            console.log('PV', this.state.primeiroValor);
            console.log('SV', this.state.segundoValor);
            const pv = parseInt(this.state.primeiroValor);
            const sv = parseInt(this.state.segundoValor);
            const op = this.state.operacao;

            let resultado = this.state.visor;

            if (op == '+') {
                resultado = pv + sv
            }

            if (op == '-') {
                resultado = pv - sv
            }

            if (op == '/') {
                resultado = pv / sv
            }

            if (op == '*') {
                resultado = pv * sv
            }

            this.setState({
                resultado: resultado,
                visor: resultado
            })
        })
    }

    limparVisor = () => {
        this.setState({ visor: 0 })
    }

    render() {
        return (
            <div>
                <h3 align="center">
                    Voitto Treinamentos<br />
                    Implemente as funcionalidades da calculadora utilizando React.js
                </h3>
                <form name="calculator">
                    <input type="textfield" name="ans" value={this.state.visor} />
                    <br />
                    <input type="button" value="1" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="2" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="3" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="+" onClick={this.operacao}/>
                    <br />
                    <input type="button" value="4" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="5" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="6" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="-" onClick={this.operacao}/>
                    <br />
                    <input type="button" value="7" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="8" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="9" onClick={this.jogarNoDisplay}/>
                    <input type="button" value="*" onClick={this.operacao}/>
                    <br />
                    <input type="button" value="0" onClick={this.jogarNoDisplay}/>
                    <input type="reset" value="c"  onClick={this.limparVisor}/>
                    <input type="button" value="/" onClick={this.operacao}/>
                    <input type="button" value="=" onClick={this.finalizarCalculo}/>
                </form>
            </div>
        );
    }
}
